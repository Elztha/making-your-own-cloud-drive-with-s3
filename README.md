# Making Your Own Cloud Drive with S3
## Overview

<div align="center">
    <img src="./images/diagram.png" width=60% >
</div>


**[s3fs](https://github.com/s3fs-fuse/s3fs-fuse)** allows Linux and macOS to mount an S3 bucket via FUSE. s3fs preserves the native object format for files, allowing use of other tools like AWS CLI.


**[Goofys](https://github.com/kahing/goofys)** allows you to mount an S3 bucket as a filey system.
It's a Filey System instead of a File System because goofys strives for performance first and POSIX second. Particularly things that are difficult to support on S3 or would translate into more than one round-trip would either fail (random writes) or faked (no per-file permission). Goofys does not have an on disk data cache (checkout catfs), and consistency model is close-to-open.

**[AWS Storage Gateway](https://aws.amazon.com/storagegateway/)** is a hybrid cloud storage service that gives you on-premises access to virtually unlimited cloud storage. Customers use Storage Gateway to simplify storage management and reduce costs for key hybrid cloud storage use cases. These include moving tape backups to the cloud, reducing on-premises storage with cloud-backed file shares, providing low latency access to data in AWS for on-premises applications, as well as various migration, archiving, processing, and disaster recovery use cases.  

## Scenario

In this lab, we will mount our S3 Bucket to an EC2 Instances using multiple methods.

First we will use s3fs, we will create a **VPC** with **EC2** infrastructure through **Cloudformation**, and then try to connect to the **EC2 instance**. After that we will launch an **S3 Bucket** and try to mount the bucket to our EC2 Instance.

Second we will use goofys, we will use the same **VPC, EC2** and **S3 Bucket** and then mount the bucket to our EC2 Instance and see the differences of both system.


Lastly we will use **AWS Storage Gateway**, we will use the same **VPC, EC2** and **S3 Bucket** and then mount the bucket to our EC2 Instance and see why both s3fs & goofys are not considered good architecture and why AWS Storage Gateway should be used if possible for presenting S3 storage as an NFS sharing system.

## Prerequisites

* Make sure your have an AWS Account

* Download the source file of this lab:
    * [NFSTutorial.yaml](./materials/NFSTutorial.yaml)

## Step by step

1. [Part 1 : S3FS](s3fs.md)

	In this part, We will mount S3 Bucket using S3FS

2. [Part 2 : Goofys](goofys.md)

	In this part, We will mount S3 Bucket using goofys

3. [Part 3 : AWS Storage Gateway](gateway.md)

	On this last part, we will mount S3 Bucket using AWS Storage Gateway

## Challenges

```

```

## Conclusion

After finishing this lab, you would had learned what

## Clean up

* **Cloudformation stack**
* **EC2 instance**
* **S3 Bucket**

## References

* https://github.com/s3fs-fuse/s3fs-fuse
* https://github.com/s3fs-fuse/s3fs-fuse/wiki/FAQ
* https://cloud.ibm.com/docs/services/cloud-object-storage/cli?topic=cloud-object-storage-s3fs
* https://medium.com/tensult/aws-how-to-mount-s3-bucket-using-iam-role-on-ec2-linux-instance-ad2afd4513ef
* https://itneko.com/linux-goofys-s3/
* https://github.com/kahing/goofys/issues/76
* https://aws.amazon.com/storagegateway/
* https://aws.amazon.com/storagegateway/pricing/
* https://aws.amazon.com/s3/pricing/