# Making Your Own Cloud Drive with S3 (Part1 : s3fs)

<p align="center">
    <img src="./images/s3fs.png" width=70% height=70%>
</p>

## Prerequisites

* Make sure your have an AWS Account

* Download the source file of this lab:
    * [NFSTutorial.yaml](./materials/NFSTutorial.yaml)

## Step by step

### Setting up your VPC & EC2 Environtment

> ***Important***

> We will make our Environment on the **N. Virginia** Region so make sure that you are already at the **N. Virginia** Region

Use the [CloudFormation template](./materials/VPC_lab1.yaml) to setup your VPC architecture

- On the **Service** menu, select [CloudFormation](https://console.aws.amazon.com/cloudformation/).

- Select **Create Stack**.

- ☑ **Template is ready**.

- In **Specify template**, ☑ **Upload a template file**.

- Select **Choose file**, and select **[NFSTutorial.yaml](./materials/NFSTutorial.yaml)** and click **Next**.


<p align="center">
    <img src="./images/1.png" width=70% height=70%>
</p>


- Input `NFSLab-Stack-1-your-name` for the **Stack name**

> Ex: `NFSLab-Stack-1-John`.

<p align="center">
    <img src="./images/2.png" width=70% height=70%>
</p>

- Choose the key file that you will use for the EC2 Instance & select **Next**.

- Leave the settings as default, select **Next**.

- Review the details of this stack, if there is no problem select **Create stack**.

### Create S3 bucket
- On the **service** menu, click **S3**.

- Click **Create Bucket**.

- For Bucket Name, type a **nfslab-stack-s3fs-your-name**.

> Ex: `nfslab-stack-s3fs-alvin`.

- For Region, choose **US East(N.Virginia)**.

<p align="center">
    <img src="./images/9.png" width=70% height=70%>
</p>


- Scroll down and click on **Create Bucket**

### Create an IAM Role to access the S3 Bucket

- On the **Service** menu, select **IAM**.

- In the navigation pane, click **Roles**.

- Click **Create policy**, then click on the **JSON** tab

- Copy and paste the IAM Policy JSON attached on [IAMPOLICY.json](./materials/IAMPOLICY.json)
> change the `{bucket-name}` part to your **nfslab-stack-s3fs-your-name** (S3 Bucket name).


<p align="center">
    <img src="./images/5.png" width=70% height=70%>
</p>

- Then, click on review, type in `s3fs-access` as the name of policy then add `Access to S3 Bucket` for the description and them click on **Create Policy**

<p align="center">
    <img src="./images/4.png" width=70% height=70%>
</p>

--

- Then in the navigation pane, click **Roles**, then click on **Create Roles**

- Pick **EC2** as the use case then click on **Next:Permissions**
<p align="center">
    <img src="./images/6.png" width=70% height=70%>
</p>

- On the **Filter Policies**, search for the **s3fs-access** & **AmazonSSMManagedInstanceCore** policy we just created and tick on it to attach

<p align="center">
    <img src="./images/7.png" width=70% height=70%>
</p>

<p align="center">
    <img src="./images/12.png" width=70% height=70%>
</p>

- Click on **Next:tags**, then click again on **Next:Review** and input your role name as `s3fs-ec2` and then click on **Create Role**

<p align="center">
    <img src="./images/8.png" width=70% height=70%>
</p>


### Attaching IAM Role to your EC2

- On the **Service** menu, select **EC2**.

- On the navigation bar, click on **Instances**

- **Add filter** for `NFS` to see your running instance, and then **right click**, choose **Instance Settings**, and then **Attach/Replace IAM Roles**

<p align="center">
    <img src="./images/10.png" width=70% height=70%>
</p>

- Choose the IAM role we created before (**s3fs-ec2**), then click **Apply**

<p align="center">
    <img src="./images/11.png" width=70% height=70%>
</p>


### Mounting your S3 Bucket to EC2

- On the **Service** menu, select **EC2**.

- On the navigation bar, click on **Instances**

- **Add filter** for `NFS` to see your running instance, and then **right click**, choose **Connect** and for the connection method pick **Session Manager**, then click on **Connect**.

- On the new SSH Session opened in new tab, install **s3fs-fuse** by typing this command

```
$ sudo su
# amazon-linux-extras install epel -y
# yum install s3fs-fuse -y
```

> You can check if s3fs is installed by typing `$ which s3fs` and it should return the directories in which you install s3fs

- Now create a directory or provide the path of an existing directory and mount S3bucket in it by typing:

```
# mkdir -p ~/s3fs-demolab
```

- Now mount the folder you created with your S3 bucket by typing this command

> Please change `nfslab-stack-s3fs-{your.name}` to the name of your S3 Bucket

```
# s3fs nfslab-stack-s3fs-{your.name} ~/s3fs-demolab \
-o iam_role="s3fs-ec2" -o url="https://s3.us-east-1.amazonaws.com" \
-o endpoint=us-east-1 \
-o dbglevel=info \
-o curldbg \
-o use_cache=/tmp \
-f

```

- Check if the bucket has been successfully mounted to your instance

```
# df -h
```

The results would show an s3fs file system mounted with size of 256 Terabytes on the instance

<p align="center">
    <img src="./images/13.png" width=70% height=70%>
</p>


### Test your mounted S3 Bucket

- Now still in the same instance, create a file by typing

```
# cd ~/s3fs-demolab
# touch go.txt
```

- Check if the file go.txt exists in your S3 Bucket

<p align="center">
    <img src="./images/14.png" width=70% height=70%>
</p>


## Conclusion

Congratulations, have learned how to prepare your VPC & EC2 with CloudFormation. Most importantly to mount your S3 Bucket to your EC2 instance, after clean up, you can proceed to [Part 2 : Goofys](goofys.md) for the next part!

## Clean up 

* **Cloudformation stack**
* **RDS database instance**
* **EC2 instance**

